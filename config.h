#ifndef CONFIG_H
#define CONFIG_H

#define LAMBDA 0.08
#define SERVICE_MIN 5
#define SERVICE_MAX 30
#define STARTING_HOURS 8
#define STARTING_MINUTES 30
#define OPEN_HOURS 9

#endif //CONFIG_H
