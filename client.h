#ifndef CLIENT_H
#define CLIENT_H

typedef struct Client_ {
    int id;
    int arrival_time;
    int service_length;
    int service_start;
    int service_end;
    struct Client_ *next;
} Client;

void printQueue(Client *queueStart);

Client *generateQueue(Client *queue);

Client *simulateQueue(Client *queue);

void writeQueue(Client *queue, char *fileName);

void stats(Client *queue);

#endif //CLIENT_H
