default: bank
client.o: client.c client.h utils.h config.h
	gcc -c client.c
utils.o: utils.c utils.h config.h
	gcc -c utils.c -lm
main.o: main.c client.h
	gcc -c main.c
bank: client.o utils.o main.o
	gcc -o bank client.o utils.o main.o -lm
clean:
	rm *.o bank
