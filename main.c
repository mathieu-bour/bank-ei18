#include <stdlib.h>
#include "client.h"


int main() {
    Client *queue = NULL;

    queue = generateQueue(queue);
    queue = simulateQueue(queue);
    printQueue(queue);
    writeQueue(queue, "rapport.csv");
    stats(queue);

    return 0;
}