#include <stdlib.h>
#include <stdio.h>
#include "client.h"
#include "utils.h"
#include "config.h"


/**
 * Print the queue
 * @param queue The queue pointer
 */
void printQueue(Client *first) {
    Client *client = first;

    while (client->next != NULL) {
        printf(
            "%d,%s,%s\n",
            client->id,
            clockToStr(client->arrival_time),
            clockToStr(client->service_start)
        );

        client = client->next;
    }
}


/**
 * Generate the client queue (arrival_time, service_length)
 * @param queue The queue pointer
 */
Client *generateQueue(Client *first) {
    int clock = 0;
    int clientCounter = 1;

    while (clock < strToClock("17h00")) {
        int expL = expLaw();

        Client *newClient = (Client *) malloc(sizeof(Client));
        newClient->id = clientCounter;
        newClient->arrival_time = clock + expL;
        newClient->service_length = serviceLength();
        newClient->service_start = -1;
        newClient->service_end = -1;
        newClient->next = NULL;

        if (first == NULL) {
            first = newClient;
        } else {
            Client *client = first;

            while (client->next != NULL) {
                client = client->next;
            }

            client->next = newClient;
        }

        clock += expL;
        clientCounter++;
    }

    return first;
}


/**
 * Simulate the queue (service_start, service_end)
 * @param queue The queue pointer
 */
Client *simulateQueue(Client *first) {
    int clock = first->arrival_time;
    Client *client = first;

    while (clock < strToClock("17h30") && client->next != NULL) {
        int a = clock + client->service_length;

        client->service_start = clock;
        client->service_end = a;

        clock = a;

        if (clock < client->next->arrival_time) {
            clock = client->next->arrival_time;
        }

        client = client->next;
    }

    return first;
}


/**
 * Simulate the queue (service_start, service_end)
 * @param queue The queue pointer
 * @param fileName The filename
 */
void writeQueue(Client *first, char *fileName) {
    FILE *f = fopen(fileName, "w");
    Client *client = first;

    if (f == NULL) {
        fprintf(stderr, "Error while opening %s", fileName);
    }


    fprintf(f,
            "id,arrival_time,service_length,service_start,service_end,arrival_time(h),service_start(h),service_end(h)\n");

    while (client->next != NULL) {
        fprintf(f, "%d,%d,%d,%d,%d,%s,%s,%s\n",
                client->id,
                client->arrival_time,
                client->service_length,
                client->service_start,
                client->service_end,
                clockToStr(client->arrival_time),
                clockToStr(client->service_start),
                clockToStr(client->service_end)
        );

        client = client->next;
    }

    fclose(f);
}


/**
 * Print the stats
 * @param queue The queue pointer
 */
void stats(Client *first) {
    // Stats
    float avgQueueSize = 0;
    int maxQueueSize = 0;
    float avgDebit;
    float unsatifiedTx = 0;
    float avgResponseTime = 0;

    int queueSize = 0;
    Client *client = first;

    while (client->next != NULL) {
        if (client->service_start == -1) {
            unsatifiedTx++;
        } else {
            Client *client2 = first;
            int clientsBefore = 0;

            while (client2->id < client->id) {
                if (client2->service_end > client->arrival_time) {
                    clientsBefore++;
                }

                client2 = client2->next;
            }

            // Update max
            if (clientsBefore > maxQueueSize) {
                maxQueueSize = clientsBefore;
            }

            avgQueueSize += clientsBefore;
            avgResponseTime += client->service_end - client->arrival_time;
        }

        client = client->next;
        queueSize++;
    }

    avgQueueSize /= queueSize;
    avgDebit = queueSize / (float) OPEN_HOURS;
    unsatifiedTx /= queueSize;
    avgResponseTime /= queueSize;

    printf("------ Stats ------\n");
    printf("Average queue size: %.1f\n", avgQueueSize);
    printf("Max queue size: %d\n", maxQueueSize);
    printf("Average debit: %.1f clients/hour\n", avgDebit);
    printf("Unsatisfied rate: %.1f%%\n", unsatifiedTx * 100);
    printf("Average response time: %.1f minutes\n", avgResponseTime);
}