#ifndef UTILS_H
#define UTILS_H

float randOne();

int expLaw();

int serviceLength();

int strToClock(char *litteral);

char *clockToStr(int clock);

#endif //UTILS_H
