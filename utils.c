#include "utils.h"
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


/**
 * Random real number generator between [0,1]
 * @return
 */
float randOne() {
    return rand() / (float) RAND_MAX;
}


/**
 * Convert a string to an integer
 * @param litteral
 * @return
 */
int strToClock(char *litteral) {
    int hours, minutes;
    sscanf(litteral, "%dh%d", &hours, &minutes);
    return (hours - STARTING_HOURS) * 60 + minutes - STARTING_MINUTES;
}


/**
 * Convert an to it readable string value
 * @param clock
 * @return
 */
char *clockToStr(int clock) {
    if (clock == -1) {
        return "N/A";
    }

    char *result = malloc(sizeof(char) * 5);
    int hours, minutes;
    char *hoursStr = malloc(sizeof(char) * 2);
    char *minutesStr = malloc(sizeof(char) * 2);

    minutes = clock % 60;
    hours = (clock - minutes) / 60;

    hours += STARTING_HOURS;
    minutes += STARTING_MINUTES;

    if (minutes >= 60) {
        minutes -= 60;
        hours++;
    }
    sprintf(hoursStr, "%d", hours);
    sprintf(minutesStr, "%d", minutes);

    if (hours < 10) {
        hoursStr[1] = hoursStr[0];
        hoursStr[0] = '0';
    }

    if (minutes < 10) {
        minutesStr[1] = minutesStr[0];
        minutesStr[0] = '0';
    }

    sprintf(result, "%sh%s", hoursStr, minutesStr);

    return result;
}


/**
 * Exponential law
 * @return
 */
int expLaw() {
    return (int) (-logf(randOne()) / LAMBDA);
}


/**
 * Generate the service, which is an integer between SERVICE_MIN and SERVICE_MAX
 * @return
 */
int serviceLength() {
    return (int) ((SERVICE_MAX - SERVICE_MIN) * randOne());
}